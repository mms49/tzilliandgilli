﻿using Pirates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace TzilliAndGilli
{
    class TGBot : Pirates.IPirateBot
    {
        private int _teamId;
        private Location _baseLocation;
        private IDictionary<Pirate, bool> _movedThisTurn;
        private IDictionary<Island, Pirate> _assignedPirates;
        private IDictionary<Pirate, bool> _attackingPirates;
        private bool _initialized = false;
        private int _minAttackDistance = 3;
        public TGBot()
        {
            

        }

        private void Init(IPirateGame state)
        {
            _baseLocation = new Location(
               state.AllMyPirates().Sum(p => p.Loc.Row) / state.AllMyPirates().Count(),
               state.AllMyPirates().Sum(p => p.Loc.Col) / state.AllMyPirates().Count()
            );

            _teamId = state.AllMyPirates()[0].Owner;

            _movedThisTurn = new Dictionary<Pirate, bool>();

            _assignedPirates = new Dictionary<Island, Pirate>();

            _attackingPirates = new Dictionary<Pirate, bool> { };

            AssignPiratesIslands(state);

            InitTurns(state);

            _initialized = true;
                    
        }

        void InitTurns(IPirateGame state)
        {
            foreach (Pirate pirate in state.AllMyPirates())
            {
                _movedThisTurn[pirate] = false;
            }
        }

        void InitAttacking()
        {
            _attackingPirates.Clear();
        }

        private void AssignPiratesIslands(IPirateGame state)
        {
            foreach (KeyValuePair<Island, Pirate> item in state.Islands().Zip(state.AllMyPirates(), (i, p) => new KeyValuePair<Island, Pirate>(i, p)))
            {
                _assignedPirates.Add(item);
            }
        }

        public void DoTurn(IPirateGame state)
        {
            if (_initialized == false) Init(state);

            InitTurns(state);

            InitAttacking();

            List<Island> islandsSortedByDistance = state.Islands();
            islandsSortedByDistance.Sort((a, b) => state.Distance(a.Loc, _baseLocation).CompareTo(state.Distance(b.Loc, _baseLocation)));

            // IEnumerable<Island> myIslandsAttacked = GetIslandsAttackedByEnemies(state);

            IEnumerable<Island> targets = islandsSortedByDistance.Where(
                    island => state.MyIslands().Contains(island) == false &&
                    GetMyCapturingIslands(state).Contains(island) == false 
               //   &&  myIslandsAttacked.Contains(island) == false
                );

            IEnumerable<Pirate> freePirates = GetFreePirates(state);

            //foreach (Pirate capturingPirate in GetCapturingPiratesCloseToEnemy(state))
            //{
            //    state.SetSail(capturingPirate, state.GetDirections(capturingPirate.Loc, getClosestEnemy(capturingPirate, state).Loc).FirstOrDefault());
            //    _attackingPirates[capturingPirate] = true;
            //}

            //foreach (Island attackedIsland in myIslandsAttacked)
            //{
            //    SendPiratesToLocation(1, new List<Pirate> { _assignedPirates[attackedIsland] }, attackedIsland.Loc, state);
            //}

            
            foreach (Island target in targets)
            {
                SendPiratesToLocation(2, GetClosestPiratesToLocation(freePirates, target.Loc, state), target.Loc, state);
            }
        }

        private IEnumerable<Island> GetIslandsAttackedByEnemies(IPirateGame state)
        {
            return state.MyIslands().Where(i => state.AllEnemyPirates().Contains(state.GetPirateOn(i)));
        }

        private void SendPiratesToLocation(int numberOfPirates, IEnumerable<Pirate> availablePirates, Location location, IPirateGame state)
        {
            foreach (Pirate p in new ArraySegment<Pirate>(availablePirates.Take(numberOfPirates).ToArray()))
            {
                IEnumerable<Direction> directions = state.GetDirections(p, location);
                _movedThisTurn[p] = true;
                state.SetSail(p, directions.FirstOrDefault());
            }
        }
        private IEnumerable<Island> GetMyCapturingIslands(IPirateGame state)
        {
            return state.NotMyIslands().Where(i => i.TeamCapturing == _teamId);
        }

        private IEnumerable<Pirate> GetFreePirates(IPirateGame state)
        {
            return state.AllMyPirates().
                Where(
                    p => state.isCapturing(p) == false &&
                    _movedThisTurn[p] == false &&
                    _attackingPirates.Keys.Contains(p) == false
                );
        }

        private IEnumerable<Pirate> GetClosestPiratesToLocation(IEnumerable<Pirate> available, Location location, IPirateGame state)
        {
            return available.OrderBy(p => state.Distance(p.Loc, location)).AsEnumerable();
        }

        private IEnumerable<Pirate> GetCapturingPiratesCloseToEnemy(IPirateGame state)
        {
            return state.AllMyPirates().
                Where(
                    p => state.Islands().
                    Select(i => i.Loc).
                    Contains(p.Loc)
                ).
                Where(
                    cp => state.AllEnemyPirates().
                    Where(e => state.Distance(e, cp) <= _minAttackDistance).Any()
                );
        }

        private Pirate getClosestEnemy(Pirate myPirate, IPirateGame state)
        {
            return state.AllEnemyPirates().OrderBy(e => state.Distance(myPirate.Loc, e.Loc)).FirstOrDefault() ?? state.AllEnemyPirates().First();
        }
    }
}
